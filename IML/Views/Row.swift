import Foundation

struct Row: Identifiable {
    var id: UUID = UUID()
    
    let firstText: String
    let secondText: String?
    
    init(firstText: String, secondText: String? = nil) {
        self.firstText = firstText
        self.secondText = secondText
    }
}

