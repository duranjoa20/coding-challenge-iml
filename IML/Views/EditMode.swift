import Foundation

public enum EditMode {
    case active
    case inactive
    
    mutating func toggle() {
        if self == .active {
            self = .inactive
        } else {
            self = .active
        }
    }
}
