#if os(macOS)
import AppKit
#elseif os(iOS)
import UIKit
#endif

import SwiftUI
import Foundation

struct ChallengeViewConstants {
    
    #if os(macOS)
    static let fontSize = NSFont.systemFontSize
    #elseif os(iOS)
    static let fontSize = UIFont.systemFontSize
    #endif
    static let font: Font = .system(size: fontSize)
    static let blueColor = Color(cgColor: CGColor(red: 0.3, green: 0.35,   blue: 0.95, alpha: 0.35))
    static let greenColor = Color(cgColor: CGColor(red: 0.3, green: 0.95,   blue: 0.35, alpha: 0.35))
    static let redColor = Color(cgColor: CGColor(red: 0.9, green: 0.35,   blue: 0.35, alpha: 0.35))
}
