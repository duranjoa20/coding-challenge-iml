import SwiftUI

struct ChallengeViewSwiftUI: View {
    @ObservedObject var viewModel: ChallengeViewModel
    @State private var showingCustomAlert = false
    @State private var showingChangeItemOrderView = false
    @State private var isEditMode: EditMode = .inactive
    @State private var inputText: String = ""

    
    var body: some View {
        NavigationStack{
            VStack {
                // Add and Edit buttons
                HStack {
                    Button(action: { showingCustomAlert = true }) {
                        Label("Add Item", systemImage: "plus")
                    }
                    .padding()
                    .buttonStyle(BorderlessButtonStyle())
                    .sheet(isPresented: $showingCustomAlert) {
                        CustomAlertView(isPresented: $showingCustomAlert, inputText: $inputText) { newText in
                            viewModel.addItem(newText)
                        }
                    }
                    Spacer()
                    NavigationLink(destination: ChangeItemOrderView(viewModel: viewModel), isActive: $showingChangeItemOrderView) {
                        Button(action: { showingChangeItemOrderView = true }) {
                            Text(isEditMode == .active ? "Done" : "Edit")
                        }
                        .padding()
                        .buttonStyle(BorderlessButtonStyle())
                    }
                }
                
                    ScrollView {
                        GeometryReader { geometry in
                        LazyVStack(alignment: .leading, spacing: 20) {
                            ForEach(viewModel.getItems(width: geometry.size.width)){ item in
                                if let secondText = item.secondText {
                                    HStack {
                                        Text(item.firstText).frame(alignment: .leading)
                                            .background(ChallengeViewConstants.blueColor)
                                            .font(ChallengeViewConstants.font)
                                        Spacer()
                                        Text(secondText).frame(alignment: .trailing)
                                            .background(ChallengeViewConstants.greenColor)
                                            .font(ChallengeViewConstants.font)
                                    }
                                    
                                } else {
                                    Text(item.firstText)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .background(ChallengeViewConstants.redColor)
                                        .font(ChallengeViewConstants.font)
                                }
                            }
                        }
                        .padding()
                    }
                }
            }
        }
    }
}

struct SingularItemListPerRow_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeViewSwiftUI(viewModel: ChallengeViewModel()).defaultPreviews()
    }
}
