//
//  ChangeItemOrderView.swift
//  IML
//
//  Created by Joaquin Duran on 30/01/2024.
//

import SwiftUI

struct ChangeItemOrderView: View {
    @ObservedObject var viewModel: ChallengeViewModel
    @State var editMode: EditMode = .inactive
    
    var body: some View {
        List {
            ForEach(viewModel.getItems(width: 0)) { item in
                Text(item.firstText).frame(maxWidth: .infinity, alignment: .leading)
            }
            .onMove(perform: { indices, newOffset in
                viewModel.moveItems(from: indices, to: newOffset)
            })
            .onChange(of: viewModel.itemTexts) { _, _ in
                
            }
        }
    }
}

#Preview {
    ChangeItemOrderView(viewModel: ChallengeViewModel())
}
