import SwiftUI
struct CustomAlertView: View {
    @Binding var isPresented: Bool
    @Binding var inputText: String
    var onSave: (String) -> Void

    var body: some View {
        VStack(spacing: 20) {
            Text("Add New Item")
                .font(.headline)

            TextField("Enter text", text: $inputText)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()

            HStack {
                Button("Cancel") {
                    isPresented = false
                }
                .padding()

                Button("Save") {
                    onSave(inputText)
                    isPresented = false
                    inputText = "" // Clear input text after saving
                }
                .padding()
            }
        }
        .padding()
        .background(Color.white)
        .cornerRadius(15)
        .shadow(radius: 10)
    }
}
