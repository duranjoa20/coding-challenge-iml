import Foundation
import Combine

class ChallengeViewModel: ObservableObject {

    @Published var itemTexts: [String] = [
        "A beautiful day in hawaii",
        "A very long item that should span the whole width",
        "Item 3",
        "Item 4",
        "Item 5",
        "Item 6",
        "Item 7",
        "Mi quis hendrerit dolor magna eget est lorem ipsum dolor. Ipsum dolor sit amet consectetur. Commodo viverra maecenas accumsan lacus vel",
        "Item 8",
        "Item 9",
        "Item 10",
        "Item 11",
        "Item 12",
        "Item 13",
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Est ullamcorper eget nulla facilisi etiam. Vulputate odio ut enim blandit volutpat maecenas volutpat blandit.",
        "14"
    ]
    func getItems(width: CGFloat) -> [Row] {
        return transforStringIntoRows(itemTexts: itemTexts, width: width)
    }
    
    func addItem(_ item: String) {
        itemTexts.append(item)
    }

    func moveItems(from source: IndexSet, to destination: Int) {
        itemTexts.move(fromOffsets: source, toOffset: destination)
    }

    private func transforStringIntoRows(itemTexts: [String], width: CGFloat) -> [Row] {
        var i = 0
        var rows: [Row] = []
        while i < itemTexts.count {
            guard let item1 = itemTexts[safe: i] else { return rows }
            guard let item2 = itemTexts[safe: i + 1] else { rows.append(Row(firstText: item1)); return rows }
            
            if item2.width + item1.width < width {
                rows.append(Row(firstText: item1, secondText: item2))
                i += 1
            } else {
                rows.append(Row(firstText: item1))
            }
            i += 1
        }
        return rows
        
    }
}
