import Foundation
import SwiftUI

extension Collection {
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

// Extension on View to add a method for generating device-oriented previews
extension View {
    func defaultPreviews() -> some View {
        return self.devicePreviews(with: [
            ("iPhone 14 Pro Max", .portrait),
            ("iPhone 14 Pro Max", .landscapeLeft),
            ("iPhone 14 Pro Max", .landscapeRight),
            ("iPad Pro (11-inch) (3rd generation)", .portrait),
            ("iPad Pro (11-inch) (3rd generation)", .landscapeRight)
        ])
    }
    func devicePreviews(with devices: [(name: String, orientation: InterfaceOrientation)]) -> some View {
        Group {
            ForEach(devices, id: \.name) { device in
                self
                    .previewDevice(PreviewDevice(rawValue: device.name))
                    .previewDisplayName("\(device.name) \(device.orientation == .portrait ? "portrait" : "landscape")")
                    .previewInterfaceOrientation(device.orientation)
            }
        }
    }
}

#if os(macOS)
import AppKit

extension String {
    var width: CGFloat {
        let font = NSFont.systemFont(ofSize: ChallengeViewConstants.fontSize)
        let attributes: [NSAttributedString.Key: Any] = [.font: font]
        let attributedString = NSAttributedString(string: self, attributes: attributes)
        let size = attributedString.size()
        return size.width
    }
}
#elseif os(iOS)
import UIKit

extension String {
    var width: CGFloat {
        let font = UIFont.systemFont(ofSize: ChallengeViewConstants.fontSize)
        let attributes = [NSAttributedString.Key.font: font]
        let attributedString = NSAttributedString(string: self, attributes: attributes)
        let size = attributedString.size()
        return size.width
    }
}
#endif
