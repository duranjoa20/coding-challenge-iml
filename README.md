# Assignment


### User Store

As a user, I want to see a list of items displayed on a simple screen in an iOS app.

### Requirements

1. Implement the screen in a native iOS app in Swift.
1. An item in the list contains text of variable length.
2. The item is sized to fit its content.
2. There are at least 10 items in the list. (The text list can be hard-coded)
3. Items should be displayed with a maximum of two per line.
    - The first per line should be left-aligned.
    - The second, if any, should be right-aligned.
5. (Optional) The screen supports both portrait and landscape orientations.



**Design Draft**

The layout of the screen should look similar to the mockup below.

![](./mockup.png)
